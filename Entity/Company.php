<?php
namespace Entity;

/**
 * Company
 *
 * @ORM\Table(name="company", uniqueConstraints={@ORM\UniqueConstraint(name="url", columns={"url"}), @ORM\UniqueConstraint(name="email", columns={"email"})}, indexes={@ORM\Index(name="FK_company_category", columns={"cat_id"})})
 * @ORM\Entity
 */
class Company extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false, options={"default"="'0'"})
     */
    public $email = '\'0\'';

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=false, options={"default"="'0'"})
     */
    public $password = '\'0\'';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=50, nullable=false)
     */
    public $url;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=50, nullable=false)
     */
    public $picture;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    public $content;
    
    public $cat_id;
	
	private $grade;

	private $reviews;

public static function tableName(){
        return 'company';


}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Company
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Company
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Company
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Company
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Company
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return integer
     */
    public function getCat_id()
    {
        return $this->cat_id;
    }

    /**
     * @param Category $cat
     * @return Company
     */
    public function setCat_id($cat)
    {
        $this->cat_id = $cat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        $allReviews = Review::findBy(['company_id'=>$this->getId()]);
        $sum = 0;
        foreach ($allReviews as $review) {$sum += $review->getGrade();}
        if ($sum!=0) {
            $avg = $sum/count($allReviews);
            return round($avg,1);} else {return 0;}
    }

    /**
     * @param mixed $grade
     * @return Company
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReviews()
    {
        $allreviews = Review::findBy(['company_id'=>$this->getId()]);

        return $allreviews;
    }

    /**
     * @param mixed $reviews
     * @return Company
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
        return $this;
    }


}