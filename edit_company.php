<?php include 'topbar.php';

if (isset($_SESSION['company'])) {
    $company = Entity\Company::find($_SESSION['user_id']);
    $name = $company->getName();
    $email = $company->getEmail();
    $photo = $company->getPicture();
    $url = $company->getUrl();
    $content=$company->getContent();
    $catid=$company->getCat_id();

}
$allcategories = Entity\Category::findBy();

?>


<div id="body" class="container" style="margin-top:20px">
    <div class='wrapper row'>
        <h3>Edit account details for:<b> <?php echo $email ?></b> </h3>
        <div class='col-sm-12 col-lg-9'>
            <div style="margin-top: 20px"></div>
            <form id="edit_company" name="edit_company" action="processEditCompany.php" method="POST">
            <div><h4>Photo: <?php echo "<img src='$photo' />" ?></h4></div>
            <div style="margin-top: 20px"></div>
            <div><label><h4>Company Name:</h4></label>
                <input type="text" id="name" placeholder="<?php echo $name ?>" name="name" class="form-control">
            </div>
                <div class='input-group mb-3' style="margin-top:20px;">
                    <div class='input-group-prepend'>
                        <label class='input-group-text' for='inputGroupSelect01'>Category</label>
                    </div>
                    <select name='cat_id' class='custom-select' id='inputGroupSelect01'>
                        <option>Choose...</option>
                        <?php foreach ($allcategories as $category) { $name=$category->getName();
                            $id=$category->getId(); if ($id==$catid) { echo"<option selected value='$id'>$name</option>";} else {
                            echo"<option value='$id'>$name</option>";}
                        } ?>

                    </select>
                </div>
            <div><label><h4>Description: </h4></label>
                <textarea rows="3" id="content" placeholder="<?php echo $content ?>" name="content" class="form-control"></textarea>
            </div>
            <div><label><h4>Website: </h4></label>
                <input type="text" id="url" placeholder="<?php echo $url ?>" name="url" class="form-control">
            </div>
                <div><label><h4>Photo Url: </h4></label>
                    <input type="text" id="picture" name="picture" class="form-control">
                </div>
                <div style="margin-top: 20px"></div>

                <a class="btn btn-outline-success" onclick="document.getElementById('edit_company').submit();">Save</a>
                <div style="margin-top: 20px"></div>
                <h3>Change Password: </h3>
                <div style="margin-top: 20px"></div>
                <div><label><h4>Old Password: </h4></label>
                    <input type="password" id="oldpass" name="oldpass" class="form-control">
                </div>
                <div><label><h4>New Password: </h4></label>
                    <input type="password" id="newpass1" name="newpass1" class="form-control">
                </div>
                <div><label><h4>New Password: </h4></label>
                    <input type="password" id="newpass2" name="newpass2" class="form-control">
                </div>

            <div style="margin-top: 20px"></div>
            <a class="btn btn-outline-success" onclick="document.getElementById('edit_company').submit();">Save</a>
            </form>
            <div style="margin-top: 20px"></div>
        </div>
 </div>


</div>
<?php include'footer.php';?>
