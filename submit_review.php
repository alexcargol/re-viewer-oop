<?php
include 'config.php';

$NewReview = new \Entity\Review();

$user_id = $_POST['user_id'];
$company_id = $_POST['company_id'];
$grade = $_POST['grade'];
$description = $_POST['description'];

if ($description =="") { header('Location: company.php?message=1&id='.$company_id.''); die(); }

$NewReview->setUser_id($user_id);
$NewReview->setCompany($company_id);
$NewReview->setGrade($grade);
$NewReview->setDescription($description);
$NewReview->save();

header('Location: company.php?id='.$company_id.'');


?>