<?php include 'topbar.php'; ?>
<?php  

		$allcategories = Entity\Category::findBy();
		
	$maxPerPage = 8;
	$currentPage = 1;
if (isset($_GET['page'])){
    $currentPage = $_GET['page'];
}
	$nrOfPages = ceil(count($allcategories)/$maxPerPage);
	$startIndex = ($currentPage-1)*$maxPerPage;
	$data = array_slice($allcategories, $startIndex, $maxPerPage);

?>


<div id="body" class="container" style="margin-top:20px">
<div name='wrapper' class='row'>
 <h3>Find secure online shops - All categories</h3> 

	               <div class='col-sm-12 col-lg-9'>
	<?php foreach ($data as $category) { $name=$category->getName(); 
                                        $subcategory= Entity\Subcategory::findBy(['cat_id'=>$category->getId()]);
                                        $cat_id = $category->getId();
	
				echo"<div class='card w-100'> 
						<div class='card-body row'>
                        <div class='col-4'><a href='category-id.php?id=$cat_id'><h4>$name</h4></a></div>
                    <div class='col-8'>";
                        foreach ($subcategory as $list) { $sname = $list->getName(); echo"
	                    <div class='d-flex p-2'><a href='category-id.php?id=$cat_id'><h6>$sname</h6></a></div>";}
                        
                echo"</div></div>
                </div><br/>";} ?>
		
        </div>
			
		<div  class="col-sm-12 col-lg-3">
            <?php include 'sidebar.php'; ?>
        </div> </div>
	
<div class="col-12">
				<nav aria-label="Page navigation example">
				  <ul class="pagination justify-content-center">
					 <?php 
		if ($currentPage>1) {echo "
			<li class='page-item'><a class='page-link' href='category.php?page=1'>&laquo;</a></li>
			<li class='page-item'> <a class='page-link' href='category.php?page="; echo $currentPage-1; echo"'>Previous</a></li>";
		}
		echo "<li class='page-item'><a class='page-link' href='#'>$currentPage</a></li>";
			if ($currentPage<$nrOfPages){ echo"
				<li class='page-item'><a class='page-link' href='category.php?page="; echo $currentPage+1; echo "'>Next</a></li>
				<li class='page-item'><a class='page-link' href='category.php?page="; echo $nrOfPages; echo"'>&raquo;</a></li>";
			} 
		?>      
				  </ul>
				</nav>	</div>	
</div>
<?php include'footer.php';?>
