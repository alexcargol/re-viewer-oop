<?php include 'topbar.php';
$allcategories = Entity\Category::findBy();

$message = false;
if (isset($_GET['message'])) {$message = intval($_GET['message']);

    if ($message == 1){ echo '<script type="text/javascript">
     alert("Please enter company name.")</script>';}
    if ($message == 2){ echo '<script type="text/javascript">
     alert("Passwords cannot be empty.")</script>';}
    if ($message == 3){ echo '<script type="text/javascript">
     alert("Passwords do not match.")</script>';}
    if ($message == 4){ echo '<script type="text/javascript">
     alert("Invalid email address.")</script>';}
    if ($message == 5){ echo '<script type="text/javascript">
     alert("You must agree to our terms & conditions to register.")</script>';}
    if ($message == 6){ echo '<script type="text/javascript">
     alert("Website field cannot be empty!")</script>';}
    if ($message == 7){ echo '<script type="text/javascript">
     alert("Please select a category.")</script>';}

}


?>
<div id="body" class="container" style="margin-top:20px; margin-bottom:10px;">

    <div class="wrapper" style="background-image: url('../img/bg-registration-form-2.jpg'); padding:20px;">
        <div class="inner">
            <form action="submitRegisterCompany.php" name="registerUser" method="post" >
                <h3 class="shadow p-3 mb-5 bg-white rounded d-flex justify-content-center" style="color:dodgerblue">Company Registration</h3>
                <div class="form-group">
                    <div class="form-wrapper">
                        <label for="">Company Name</label>
                        <input name="name" type="text" class="form-control" value="<?php if (isset($_SESSION['name'])){echo $_SESSION['name'];}?>">
                    </div>

                    <div class="form-wrapper">
                        <label for="">Website http://</label>
                        <input name="url" type="text" class="form-control" value="<?php if (isset($_SESSION['url'])){echo $_SESSION['url'];}?>">
                        </div>


                        <div class='input-group mb-3' style="margin-top:20px;">
                                             <div class='input-group-prepend'>
                            <label class='input-group-text' for='inputGroupSelect01'>Category</label>
                          </div>
                          <select name='cat_id' class='custom-select' id='inputGroupSelect01'>
                            <option selected>Choose...</option>
                             <?php foreach ($allcategories as $category) { $name=$category->getName();
                              $id=$category->getId();
                              echo"<option value='$id'>$name</option>"; } ?>

                          </select>
                        </div>
                </div>
                <div class="form-wrapper">
                    <label for="">Email</label>
                    <input name="email" type="text" class="form-control" value="<?php if (isset($_SESSION['email'])){echo $_SESSION['email'];}?>">
                </div>
                <div class="form-wrapper">
                    <label for="">Password</label>
                    <input name="password_1" type="password" class="form-control">
                </div>
                <div class="form-wrapper">
                    <label for="">Confirm Password</label>
                    <input name="password_2" type="password" class="form-control">
                </div>
                <div class="checkbox">
                    <label>
                        <input name="terms" type="checkbox" value="yes" checked> I accept the Terms of Use & Privacy Policy.
                        <span class="checkmark"></span>
                    </label>
                </div>
                <button  type="submit" class="btn btn-success">Register Now</button>
            </form>
        </div>
    </div>
</div>
<?php include 'footer.php'?>
