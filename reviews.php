<?php include 'topbar.php';

if (isset($_SESSION['username'])) {
    $username = Entity\User::find($_SESSION['user_id']);
    $allreviews = Entity\Review::findBy(['user_id'=>$_SESSION['user_id']]);
    $name = $username->getName();
}

if (isset($_SESSION['company'])) {
    $company = Entity\Company::find($_SESSION['user_id']);
    $allreviews = $company->getReviews();
    $name = $company->getName();
}
if (count($allreviews)==0) {$message = "<h3>There are no reviews yet!</h3>";}

?>

<div id="body" class="container" style="margin-top:20px">
    <div id="wrapper" class='row'>
        <h3>My Reviews - <?php echo $name ?></h3>
        <div class='col-sm-12 col-lg-12'>
            <?php if (isset($message)) {echo $message;} else {
                foreach ($allreviews as $review):
            $user = Entity\User::find($review->getUser_id());
            $user_name = $user->getName();
            $photo = $user->getPhoto();
            $company= Entity\Company::find($review->getCompany());
            $reviewGrade = $review->getGrade();
            $reviewContent = $review->getDescription();

            ?>
            <div class="comment-box row" >
                <li class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object rounded-circle" src="<?php echo $photo; ?>" alt="profile">
                    </a>
                    <div class="media-body">
                        <div class="row">
                            <h4 class="media-heading text-uppercase reviews"><?php echo $user_name; ?></h4>
                            <div class="ml-auto p-2" style="margin-right:10px;">
                                <h4><B><a href="company.php?id=<?php echo $company->getId();?>"><?php echo $company->getName(); ?></a></B></h4>
                                <ul class="list-unstyled list-inline rating mb-0" style="color:gold;">
                                    <?php
                                    for ($i=1; $i<=$reviewGrade; $i++) {
                                        echo "<li class=\"list-inline-item mr-0\"><i class=\"fas fa-star amber-text\"> </i></li>";
                                    }
                                    if (($reviewGrade*10)%2 > 0 ) { echo "<li class=\"list-inline-item\"><i class=\"fas fa-star-half-alt amber-text\"></i></li>"; }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="well well-lg">

                            <p class="media-comment font-weight-light">
                                <?php echo $reviewContent ?>
                            </p>

                        </div>
                    </div>

                </li>

            </div>
            <?php endforeach; } ?>

        </div>

         </div>


</div>
<?php include'footer.php';?>
