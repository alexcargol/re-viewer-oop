

<div class="container" style="margin-top:10px; margin-bottom:10px;">
    <!--Section description-->
    <div class="row">

        <!--Grid column-->
        <div class="col-lg-12 col-md-12 mb-md-0 mb-5">
            <form id="reviewForm" name="reviewForm" action="submit_review.php"  method="POST">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id'] ?>" class="form-control">

                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="hidden" id="company_id" name="company_id" value="<?php echo $id ?>" class="form-control">

                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->


                        <div style="margin-top:10px;">
                            <div><h3>Chose Rating</h3></div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="grade" id="exampleRadios5" value="5" checked>
                                5 <label class="form-check-label" for="exampleRadios5" style="color:gold">
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="grade" id="exampleRadios4" value="4"> 4
                                <label class="form-check-label" for="exampleRadios4" style="color:gold">
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="grade" id="exampleRadios3" value="3"> 3
                                <label class="form-check-label" for="exampleRadios3" style="color:gold">
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="grade" id="exampleRadios2" value="2"> 2
                                <label class="form-check-label" for="exampleRadios2" style="color:gold">
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="grade" id="exampleRadios1" value="1"> 1
                                <label class="form-check-label" for="exampleRadios1" style="color:gold">
                                    <i class="fas fa-star"></i>
                                </label>
                            </div>
                        </div>
                    <br>

                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea id="description" name="description" rows="3" class="form-control md-textarea"></textarea>
                            <label for="message">Your message</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->
                <button class="btn btn-outline-primary" type="submit" >Submit</button>
                </form>


            </div>
            <!--Grid column-->



</div>
</div>
