<?php include '../config.php'; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title>BuySafe - Customer Reviews</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/ade0e905b9.js" crossorigin="anonymous"></script>
<link rel="icon" type="image/png"  href="../img/logo.png">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>
<body>
    <nav id="topbar" style="margin: 0 0; background: #011f4b; " class="navbar navbar navbar-light">
        
			<img src="../img/Trust-icon2transparent2.png" />
            <p> 
            <p>
                <?php if (isset($user)) {
                    echo "$user
							<a class='navbar-brand btn btn-outline-warning' style='color:white' href='dashboard/logout.php'><b>Logout</B></a>";
                } else {
                    echo "<nav class='d-flex ml-auto p-2 navbar'>
					<div class='btn-group' role='group'>
				<a type='button' href='user.php' class='btn btn-outline-light' style='color:cornflowerblue'>Shoppers</a>
				<a type='button' href='company.php' class='btn btn-outline-primary' style='color:white'>Retailers</a>
				</div></nav>";
                } ?>      

      
    </nav>
    <div id="menu" class="col-12 row menu" >
       <nav class="navbar navbar-expand-lg navbar-light" style="background:#03396c">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">	
			<nav class='d-flex ml-auto p-2 navbar'>
					<div class='btn-group' role='group'>
					<a type='button' class='btn btn-outline-primary' href='../index.php' style="color:white">Home</a>
					<a type='button' class='btn btn-outline-primary' href='../companies.php' style="color:white">Companies</a>
					<a type='button' class='btn btn-outline-primary' href='../services.php' style="color:white">Services</a>
					<a type='button' class='btn btn-outline-primary' href='../contact.php' style="color:white">Contact</a>
				</div></nav>
  </nav>
</div>
	