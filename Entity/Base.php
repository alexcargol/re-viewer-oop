<?php
namespace Entity;
/**
 * Base namespace Entity\.
 *
 */

abstract class Base
{
public $id;

    /**
     * Base constructor.
     * @param $id
     */

    public abstract static function tableName();

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Base
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function delete(){
    query("DELETE FROM ".static::tableName()." WHERE id=".intval($this->getId()));
    }

    public function create(){
        $values = get_object_vars($this);
        unset($values['id']);
        $keys = implode('`,`',array_keys($values));
        $vals = implode("', '", $values);

        $id = query("INSERT INTO ".static::tableName()." (`".$keys."`) VALUES ('".$vals."')");
        $this->setId($id);
    }

    private function update(){
        $fields=[];
        $values = get_object_vars($this);
        unset($values['id']);
        foreach ($values as $key => $value) {
            $fields[] = "$key='$value'";
        }
        $sets = implode(', ', $fields);
        query("UPDATE ".static::tableName()." SET ".$sets." WHERE id=".intval($this->getId()));
    }

    public function save(){
        if (is_null($this->getId())){
            $this->create();
        } else {
            $this->update();
        }
    }

    static public function findBy($filters=[], $orderBy=null, $orderDirection='ASC', $limit=0, $offset=0){

        $query = "SELECT * FROM ".static::tableName()." ";

        if (count($filters)>0) {
            $fields=[];
            foreach ($filters as $key => $value) {
                $fields[] = "$key='$value'";
            }
            $where = implode(' AND ', $fields);

            $query .= " WHERE $where ";
        }

        if (!is_null($orderBy)){
            $query.= " ORDER BY $orderBy $orderDirection";
        }

        if ($limit>0){
            $query.=" LIMIT $offset,$limit";
        }

        $data = query($query);

        $objects =[];
        $className = static::class;
        foreach ($data as $line){
            $objects[] = static::fromArray($line);
        }

        return $objects;
    }

    static public function findOneBy($filters=[], $orderBy=null, $orderDirection='ASC')
    {
        $objects = static::findBy($filters, $orderBy, $orderDirection);

        return $objects[0];

    }

    static public function find($id)
    {
        return static::findOneBy(['id'=>$id]);
    }
    /**
     * @return mixed
     */

    public function getCategories()
    {
        $list = [];
        $cat = query ("SELECT * from category");
        foreach ($cat as $category) { $list[]= Category::fromArray($category); }
        return $list;
    }

    static public function fromArray($array)
    {
        $className = static::class;
        $object = new $className();

        foreach ($array as $key => $value)
        {
            $object->$key = $value;
        }
        return $object;
    }
}