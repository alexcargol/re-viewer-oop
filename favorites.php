<?php include 'topbar.php';

if (isset($_SESSION['username'])) {
    $username = Entity\User::find($_SESSION['user_id']);
    $allfavs = Entity\Favorites::findBy(['user_id'=>$_SESSION['user_id']]);
    $name = $username->getName();
}

if (count($allfavs)==0) {$message = "<h3>There are no favorites yet!</h3>";}

?>

<div id="body" class="container" style="margin-top:20px; margin-bottom:20px;">
    <div id="wrapper" class='row'>
        <h3>My Favorites - <?php echo $name ?></h3>
        <div class='col-sm-12 col-lg-12'>
            <?php if (isset($message)) {echo $message;} else {
                foreach ($allfavs as $fav):
                    $this_id=$fav->getId();
            $company_id=$fav->getCompanyId();
            $company= Entity\Company::find($company_id);
            $company_name = $company->getName();
            $grade = $company->getGrade();
            $reviewNr = count($company->getReviews());
            $cat_id=$company->getCat_id();
            $cat_name= Entity\Category::find($cat_id)->getName();
            $content = $company->getContent();
            $photo = $company->getPicture();
            ?>
                    <div class='card w-100'>
                        <div class='card-body row'>
                            <div class='col-lg-4 col-md-12 col-sm-12'> <img src='<?php echo $photo?>' width='350px' height='150px' /></div>
                            <div class='col-lg-8 col-md-12 col-sm-12'>
                                <div class='d-flex flex-row'>
                                    <div class='mr-auto p-2'><a href='company.php?id=<?php echo $company_id?>'><h4><?php echo $company_name ?></h4></a></div>
                                    <div class='p-2'><a class='Revbox'  href='company.php?id=<?php echo $company_id?>' title='Write a review'><span><i class='fas fa-comments'></i></span></a></div>
                                    <div class='p-2'><a class='Favoritebox' href='?delfav=<?php echo $this_id ?>' style='color:red' title='Remove from favorites'><span class='fa fa-heart'></span></a></div>
                                </div>
                                <div class='d-flex flex-row'><a href='category-id.php?id=<?php echo $cat_id?>'><p><?php echo $cat_name ?></p></a></div>
                                <div class='d-flex flex-row'><i class='fas fa-shield-alt'> Trust score: <?php echo $grade?></i>&nbsp;based on&nbsp;<i class='fas fa-user-shield'> <?php echo $reviewNr?> Reviews</i><b> </b></div>
                                <div class='d-flex flex-row'><?php echo $content ?></div>
                            </div></div>
                    </div><br/>
            <?php endforeach; } ?>

        </div>

         </div>


</div>
<?php include'footer.php';?>
