<?php include 'topbar.php'; ?>
<?php  
 

$allcompanies = Entity\Company::findBy();  
// { $allcompanies = Entity\Company::findBy(['cat_id' => $id]); 
		


$maxPerPage = 8;
	$currentPage = 1;
if (isset($_GET['page'])){
    $currentPage = $_GET['page'];
}
	$nrOfPages = ceil(count($allcompanies)/$maxPerPage);
	$startIndex = ($currentPage-1)*$maxPerPage;
	$data = array_slice($allcompanies, $startIndex, $maxPerPage);

?>


<div id="body" class="container" style="margin-top:20px">
<div name='wrapper' class='row'>
<h3>Find secure online shops - All companies</h3>
	               <div class='col-sm-12 col-lg-9'>
	<?php foreach ($data as $company) {
	                    $name=$company->getName();
			            $cat_id = $company->getCat_id();
						$category = Entity\Category::findOneBy(['id'=> $cat_id]);
                        $company_id= $company->getId();
						$cat_name = $category->getName();
						$grade = $company->getGrade();
						$reviewNr = count($company->getReviews());
							echo" <div class='card w-100'> 
						<div class='card-body row'>
                        <div class='col-4'> <img alt='$name' src='img/1.jpg' width='100%' /></div>
                    <div class='col-8'>
	<div class='d-flex flex-row'>
	                    <div class='mr-auto p-2'><a href='company.php?id=$company_id'><h4>$name</h4></a></div>
	                    "; if (isset($_SESSION['username'])) { echo "
	                    <div class='p-2'><a class='Revbox'  href='company.php?id=$company_id' title='Write a review'><span><i class='fas fa-comments'></i></span></a></div>
	                   <div class='p-2'><a class='Favoritebox'  href='?fav=$company_id'  title='Add to favorite'><span class='fa fa-heart'></span></a></div>";} echo "
	                  </div>
	                    
                        <div class='d-flex flex-row'><a href='category-id.php?id=$cat_id'><p>$cat_name</p></a></div>
                        <div class='d-flex flex-row'><i class='fas fa-shield-alt'> Trust score: $grade</i>&nbsp;based on&nbsp;<i class='fas fa-user-shield'> $reviewNr Reviews</i><b> </b></div>
                        <div class='d-flex flex-row'>$company->content</div>
                    </div></div>
                </div><br/>";} ?>
		
        </div>
			
		<div  class="col-sm-12 col-lg-3">
            <?php include 'sidebar.php'; ?>
        </div> </div>
	
<div class="col-12">
				<nav aria-label="Page navigation example">
				  <ul class="pagination justify-content-center">
					 <?php 
		if ($currentPage>1) {echo "
			<li class='page-item'><a class='page-link' href='companies.php?page=1'>&laquo;</a></li>
			<li class='page-item'> <a class='page-link' href='companies.php?page="; echo $currentPage-1; echo"'>Previous</a></li>";
		}
		echo "<li class='page-item'><a class='page-link' href='#'>$currentPage</a></li>";
			if ($currentPage<$nrOfPages){ echo"
				<li class='page-item'><a class='page-link' href='companies.php?page="; echo $currentPage+1; echo "'>Next</a></li>
				<li class='page-item'><a class='page-link' href='companies.php?page="; echo $nrOfPages; echo"'>&raquo;</a></li>";
			} 
		?>      
				  </ul>
				</nav>	</div>	
</div>
<?php include'footer.php';?>
