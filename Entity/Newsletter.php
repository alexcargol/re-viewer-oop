<?php


namespace Entity;


class Newsletter extends Base
{
    public $id;

    public $email;

    public static function tableName(){
        return 'newsletter';}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Newsletter
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Newsletter
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


}