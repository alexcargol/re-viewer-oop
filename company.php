<?php include 'topbar.php';
$id = intval($_GET['id']);
$company = Entity\Company::find($id);
$name = $company->getName();
$category = Entity\Category::find($company->getCat_id());
$cat_name = $category->getName();
$allreviews = $company->getReviews();
$grade = $company->getGrade();
$nrReviews = count($allreviews);
$content = $company->getContent();
$url = $company->getUrl();
$photo = $company->getPicture();

if (isset($_GET['message'])) {$message=$_GET['message'];
        if ($message==1) {echo '<script type="text/javascript">
		alert("Please enter a description for the review!")
		</script>';}
}

?>
<div id="body" class="container" style="margin-top:20px">

		<div class="row">
			<div class="category-container col-sm-12 col-lg-9">
                <div class="d-flex">
                    <div class="mr-auto p-2"><h3><?php echo $name ?></h3></div>
                    <div class="p-2">
                        <ul class="list-unstyled list-inline rating mb-0" style="color:gold;">
                            <li class="fas fa-shield-alt" style="color:mediumseagreen">Trust score:</li><?php echo "<b>$grade</b>";
                            for ($i=1; $i<=$grade; $i++) {
                                echo "<li class=\"list-inline-item mr-0\"><i class=\"fas fa-star amber-text\"> </i></li>";
                            }
                            if (($grade*10)%10 != 0 ) { echo "<li class=\"list-inline-item\"><i class=\"fas fa-star-half-alt amber-text\"></i></li>"; }
                            ?>
                        </ul></div>
                </div>
                <div class="d-flex">
                    <div class="mr-auto p-2"> <h4><?php echo $cat_name ?></h4></div>
                    <div class="p-2"><b> (<?php echo $nrReviews ?>)</b><i class="fas fa-user-shield"> Reviews</i></div>
                </div>
			
							<!-- Card -->
				<div class="card booking-card" style="width:100%">

				  <!-- Card image -->
				  <div class="view overlay">
					<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/8-col/img (5).jpg" alt="Card image cap">
					<a href="#!">
					  <div class="mask rgba-white-slight"></div>
					</a>
				  </div>

				  <!-- Card content -->
				  <div class="card-body">

					<!-- Title -->
					<h4 class="card-title font-weight-bold"><a><?php echo $name ?></a></h4>
					<!-- Data -->
                      <ul class="list-unstyled list-inline rating mb-0" style="color:gold;">
                           <?php echo "<b>$grade</b>";
                           for ($i=1; $i<=$grade; $i++) {
                            echo "<li class=\"list-inline-item mr-0\"><i class=\"fas fa-star amber-text\"> </i></li>";
                           }
                           if (($grade*10)%2 > 0 ) { echo "<li class=\"list-inline-item\"><i class=\"fas fa-star-half-alt amber-text\"></i></li>"; }
                           ?> <span style="color:black"><?php echo "<b>($nrReviews)</b>" ?></span>
                      </ul>
					<p class="mb-2">$ • <?php echo $cat_name ?></p>
					<!-- Text -->
					<p class="card-text"><?php echo $content ?></p>
					<hr class="my-4">
					<p class="lead"><strong>Program</strong></p>
					<ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
					  <li class="list-inline-item mr-0">
						<div class="chip mr-0">5:30PM</div>
					  </li>
					  <li class="list-inline-item mr-0">
						<div class="chip deep-purple white-text mr-0">7:30PM</div>
					  </li>
					  <li class="list-inline-item mr-0">
						<div class="chip mr-0">8:00PM</div>
					  </li>
					  <li class="list-inline-item mr-0">
						<div class="chip mr-0">9:00PM</div>
					  </li>
					</ul>
					<!-- Button -->
					<a href="http://<?php echo $url ?>" class="btn btn-outline-success">Website</a>

				  </div>

				</div>
				<!-- Card --><br \>
                <div class="col-lg-12">
                    <?php if (isset($_SESSION['username']) && ($_SESSION['type']=='user')) {
                        echo"<div class='col-lg-12'><h3>Submit your review</h3></div>";
                        include 'review_form.php'; }?>
                </div>



				<div class="col-lg-12"><br \><h3>Latest comments...</h3><br \></div>
				<!-- Comments Box -->
                    <?php foreach ($allreviews as $review):
                    $user = Entity\User::find($review->getUser_id());
                    $user_name = $user->getName();
                    $photo = $user->getPhoto();
                    $UserNrReviews = count($user->getReviews());
                    $reviewGrade = $review->getGrade();
                    $reviewContent = $review->getDescription();

                    ?>
 				    <div class="comment-box row" >
				    <li class="media">
                        <a class="pull-left" href="#">
                          <img class="media-object rounded-circle" src="<?php echo $photo; ?>" alt="profile">
                        </a>
                        <div class="media-body"> 
								<div class="row">
								<h4 class="media-heading text-uppercase reviews"><?php echo $user_name; ?></h4><span id="pill" class="badge badge-pill badge-primary"><?php echo $UserNrReviews ?> Reviews</span>
								<div class="ml-auto p-2" style="margin-right:10px;">
                                    <ul class="list-unstyled list-inline rating mb-0" style="color:gold;">
                                        <?php
                                        for ($i=1; $i<=$reviewGrade; $i++) {
                                            echo "<li class=\"list-inline-item mr-0\"><i class=\"fas fa-star amber-text\"> </i></li>";
                                        }
                                        if (($reviewGrade*10)%2 > 0 ) { echo "<li class=\"list-inline-item\"><i class=\"fas fa-star-half-alt amber-text\"></i></li>"; }
                                        ?>
                                    </ul>
                                </div>
								</div>
                          <div class="well well-lg">

                              <p class="media-comment font-weight-light">
                                <?php echo $reviewContent ?>
                              </p>
                             
                          </div>              
                        </div>
                     
                        </li>

				    </div>

    <?php endforeach; ?>
				

				<!-- Comment Box -->
				</div>
			<div  class="col-sm-12 col-lg-3">
			<?php include 'sidebar.php'; ?>
				</div>
		</div>

</div>
<?php include'footer.php';?>