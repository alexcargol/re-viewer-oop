<?php include 'topbar.php'; ?>

 <div id="body" class="container" style="margin-top:20px; margin-bottom:10px;">
            <div class="row justify-content-center align-items-center" style="margin-bottom:20px">
                    <h1>Select your company`s desired plan</h1>
            </div>
           <div class="row justify-content-center align-items-center">
                <div class="col-4" style="margin-bottom:20px; margin-top:20px;">
                        <h3 style="color:black">Free Plan</h3>
                    <ul class="cat_wrapper"><li> Free registration</li>
                        <li> Limited to 100 Reviews</li>
                        <li> Custom detail</li>
                        <li> Custom detail</li>
                        <li> Custom detail</li>
                    </ul>

                </div>
                <div class="col-4" style="margin-bottom:20px; margin-top:20px;">
                    <h3 style="color:darkgoldenrod">Golden Plan</h3>
                    <ul class="cat_wrapper"><li> 10$ / month</li>
                        <li> Unlimited reviews</li>
                        <li> Featured 7 days</li>
                        <li> Custom detail</li>
                        <li> Custom detail</li>
                    </ul>
                </div>
                <div class="col-4" style="margin-bottom:20px; margin-top:20px;">
                    <h3 style="color:lightgray">Platinum Plan</h3>
                    <ul class="cat_wrapper"><li> 20 $ / month</li>
                        <li> Unlimited reviews</li>
                        <li> Featured all month</li>
                        <li> Custom detail</li>
                        <li> Custom detail</li>
                    </ul>
                </div>
           </div>


    </div>

<?php include'footer.php';?>