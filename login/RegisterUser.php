<?php include 'topbar.php';
$message = false;
if (isset($_GET['message'])) {$message = intval($_GET['message']);

if ($message == 1){ echo '<script type="text/javascript">
     alert("Please enter first name and last name.")</script>';}
if ($message == 2){ echo '<script type="text/javascript">
     alert("Passwords cannot be empty.")</script>';}
if ($message == 3){ echo '<script type="text/javascript">
     alert("Passwords do not match.")</script>';}
if ($message == 4){ echo '<script type="text/javascript">
     alert("Invalid email address.")</script>';}
                            }

?>
<div id="body" class="container" style="margin-top:20px; margin-bottom:10px;">

    <div class="wrapper" style="background-image: url('../img/bg-registration-form-2.jpg'); padding:20px;">
        <div class="inner">
            <form action="submitRegisterUser.php" name="registerUser" method="post" >
                <h3 class="shadow p-3 mb-5 bg-white rounded d-flex justify-content-center" style="color:dodgerblue">Customer Registration Form</h3>
                <div class="form-group">
                    <div class="form-wrapper">
                        <label for="">First Name</label>
                        <input name="fname" type="text" class="form-control" value="<?php if (isset($_SESSION['fname'])){echo $_SESSION['fname'];}?>">
                    </div>
                    <div class="form-wrapper">
                        <label for="">Last Name</label>
                        <input name="lname" type="text" class="form-control" value="<?php if (isset($_SESSION['lname'])){echo $_SESSION['lname'];}?>">
                    </div>
                </div>
                <div class="form-wrapper">
                    <label for="">Email</label>
                    <input name="email" type="text" class="form-control" value="<?php if (isset($_SESSION['email'])){echo $_SESSION['email'];}?>">
                </div>
                <div class="form-wrapper">
                    <label for="">Password</label>
                    <input name="password_1" type="password" class="form-control">
                </div>
                <div class="form-wrapper">
                    <label for="">Confirm Password</label>
                    <input name="password_2" type="password" class="form-control">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="yes" checked> I accept the Terms of Use & Privacy Policy.
                        <span class="checkmark"></span>
                    </label>
                </div>
                <button  type="submit" class="btn btn-success">Register Now</button>
            </form>
        </div>
    </div>
</div>
<?php include 'footer.php'?>
