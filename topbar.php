<?php include 'config.php';
$newsmessage = false;
if (isset($_SESSION['username'])) {
    $username = Entity\User::find($_SESSION['user_id']);
    $user = $username->getName();
    $photo=$username->getPhoto();

    }

if (isset($_SESSION['company'])) {
    $company = Entity\Company::find($_SESSION['user_id']);
    $user = $_SESSION['company'];
    $photo=$company->getPicture();

}

if (isset($_GET['fav']))
{ $favid = intval($_GET['fav']);
    if (isset($username))
    {
        $isfavorite = Entity\Favorites::findBy(['user_id' => $_SESSION['user_id'], 'company_id'=>$favid]);
        if (count($isfavorite)==0) {$newfav = new Entity\Favorites();
                                    $newfav->setCompanyId($favid);
                                    $newfav->setUserId( $_SESSION['user_id']);
                                    $newfav->save(); echo '<script type="text/javascript">
		                                                    alert("Company was added to favorites.")</script>'; } else {echo '<script type="text/javascript">
		                                                    alert("This company was already to favorites.")</script>';}

    } else {echo '<script type="text/javascript">
		alert("Please login to add favorites")</script>';}

}

if (isset($_GET['delfav'])) {
    $fav=Entity\Favorites::find($_GET['delfav']); $fav->delete();
    echo '<script type="text/javascript">
     alert("Company was deleted from favorites.")</script>';
}

if (isset($_GET['newsletter']) && ($_GET['newsletter']==1)) {
    $newEmail = $_POST['email'];

    if (!filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {
        echo '<script type="text/javascript">
     alert("Invalid email format.")</script>';
    } else {
    $allclients = Entity\Newsletter::findBy();
    foreach ($allclients as $client) { $thisEmail = $client->getEmail();
        if ($thisEmail == $newEmail ) { $newsmessage = 1;}
    }
        if ($newsmessage != false) { echo '<script type="text/javascript">
     alert("You are already registered for newsletter.")</script>'; }
        else {    $newsletterClient = new Entity\Newsletter();
    $newsletterClient->setEmail($newEmail);
    $newsletterClient->save();
            echo '<script type="text/javascript">
     alert("Thank you for subscribing to our newsletter.")</script>';
        }
            }
    }

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title>BuySafe - Customer Reviews</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/ade0e905b9.js" crossorigin="anonymous"></script>
<link rel="icon" type="image/png"  href="img/logo.png">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>
<body>
    <nav id="topbar" style="margin: 0 0; background: #011f4b; " class="navbar navbar navbar-light">
        
			<img src="img/Trust-icon2transparent2.png" />
            <p> 
            <p>
                <?php if (isset($user)) {
                        echo"<div class='d-flex ml-auto p-2 navbar' style='color:white'>
                        <b>$user&nbsp;</b>
                        <div class=\"dropdown\">
  <button class=\"btn btn-outline-info dropdown-toggle\" type=\"button\" id=\"dropdownMenu2\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <img class='rounded-circle' src='$photo' width='50px' alt='User' />
  </button>
  <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu2\">
    <a class=\"dropdown-item\" type=\"button\" href=\"dashboard.php\">My Account</a>
    <a class=\"dropdown-item\" type=\"button\" href=\"reviews.php\">My Reviews</a>
  "; if (isset($_SESSION['username'])) { echo "
  <a class=\"dropdown-item\" type=\"button\" href=\"favorites.php\">My Favorites</a>";} echo "
  </div>
</div>
                        </div>";
                    echo "<a class='navbar-brand btn btn-outline-warning' style='color:white' href='login/logout.php'><b>Logout</B></a>";
                } else {
                    echo "<nav class='d-flex ml-auto p-2 navbar'>
					<div class='btn-group' role='group'>
				<a type='button' href='login/user.php' class='btn btn-outline-light' style='color:cornflowerblue'>Shoppers</a>
				<a type='button' href='login/company.php' class='btn btn-outline-primary' style='color:white'>Retailers</a>
				</div></nav>";
                } ?>      

      
    </nav>
    <div id="menu" class="col-12 row menu" >
       <nav class="navbar navbar-expand-lg navbar-light" style="background:#03396c">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">	
			<nav class='d-flex ml-auto p-2 navbar'>
					<div class='btn-group' role='group'>
					<a type='button' class='btn btn-outline-primary' href='index.php' style="color:white">Home</a>
					<a type='button' class='btn btn-outline-primary' href='companies.php' style="color:white">Companies</a>
                        <a type='button' class='btn btn-outline-primary' href='category.php' style="color:white">Categories</a>
					<a type='button' class='btn btn-outline-primary' href='services.php' style="color:white">Services</a>
					<a type='button' class='btn btn-outline-primary' href='contact.php' style="color:white">Contact</a>
				</div></nav>
  </nav>
        <nav class="d-flex ml-auto p-2 navbar">
            <form class="form-inline" name="menuSearch" action="find.php" method="post" >
                <input class="form-control mr-sm-2" type="text" placeholder="Search for..." name="keyword" value="" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
            </form>
        </nav>
    </div>
	