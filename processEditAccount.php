<?php include 'config.php';

$username = Entity\User::find($_SESSION['user_id']);

if (isset($_POST['fname']) && $_POST['fname']!='') {
    $username->setFirstName($_POST['fname']);
}
if (isset($_POST['lname']) && $_POST['lname']!='') {
    $username->setLastName($_POST['lname']);
}
if (isset($_POST['address']) && $_POST['address']!='') {
    $username->setAddress($_POST['address']);
}
if (isset($_POST['city']) && $_POST['city']!='') {
    $username->setCity($_POST['city']);
}
if (isset($_POST['zip']) && $_POST['zip']!='') {
    $username->setZip($_POST['zip']);
}
if (isset($_POST['country']) && $_POST['country']!='') {
    $username->setCountry($_POST['country']);
}
if (isset($_POST['photo']) && $_POST['photo']!='' ) {
    $username->setPhoto($_POST['photo']);
}
if (isset($_POST['oldpass']) && $_POST['oldpass']!='' && isset($_POST['newpass1']) && isset($_POST['newpass2']) && ($_POST['newpass1']!='') && ($_POST['newpass2']!='') && ($_POST['newpass1'] == $_POST['newpass2']) && ($username->getPassword() == md5($_POST['oldpass']))) {
    $username->setPassword(md5($_POST['newpass1']));

}

$username->save();
header('Location: dashboard.php?message=1');

?>