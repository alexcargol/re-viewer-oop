<?php
namespace Entity;
 
/**
 * Review
 *
 * @ORM\Table(name="review", indexes={@ORM\Index(name="FK_review_user", columns={"user_id"}), @ORM\Index(name="FK_review_company", columns={"company_id"})})
 * @ORM\Entity
 */
class Review extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int
     *
     * @ORM\Column(name="grade", type="integer", nullable=false)
     */
    public $grade = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50, nullable=false, options={"default"="'0'"})
     */
    public $description = '\'0\'';

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    public $company_id;

    /**
     * @var integer
     */
    public $user_id;
	
public static function tableName(){
        return 'review';}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Review
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param int $grade
     * @return Review
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Review
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company_id;
    }

    /**
     * @param Company $company
     * @return Review
     */
    public function setCompany($company)
    {
        $this->company_id = $company;
        return $this;
    }

    /**
     * @return integer
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * @param \User $user
     * @return Review
     */
    public function setUser_id($user)
    {
        $this->user_id = $user;
        return $this;
    }

}
