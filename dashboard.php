<?php include 'topbar.php';

if (isset($_GET['message'])) {$message=$_GET['message'];
    if ($message==1) {echo '<script type="text/javascript">
		alert("Your changes have been saved!")
		</script>';}
}

if (isset($_SESSION['username'])) {
    $username = Entity\User::find($_SESSION['user_id']);
    $fname = $username->getFirstName();
    $lname = $username->getLastName();
    $address = $username->getAddress();
    $email = $username->getEmail();
    $city = $username->getCity();
    $zip = $username->getZip();
    $country = $username->getCountry();
    $photo = $username->getPhoto();
    }

if (isset($_SESSION['company'])) {
    $company = Entity\Company::find($_SESSION['user_id']);
    $category= Entity\Category::find($company->getCat_id())->getName();
    $name = $company->getName();
    $url = $company->getUrl();
    $email = $company->getEmail();
    $content = $company->getContent();
    $photo = $company->getPicture();
}


?>


<div id="body" class="container" style="margin-top:20px">
    <div class='wrapper row'>
        <h3>Account details for <?php echo $email ?> </h3>
        <div class='col-sm-12 col-lg-12'>
           <?php if (isset($_SESSION['username'])):?>
            <div style="margin-top: 20px"></div>
            <div><h4>Photo: <?php echo "<img src='$photo' width='20%'/>" ?></h4></div>
            <div style="margin-top: 20px"></div>
            <div><h4>First Name: <?php echo $fname ?></h4></div>
            <div><h4>Last Name: <?php echo $lname ?></h4></div>
            <div><h4>Address: <?php echo $address ?></h4></div>
            <div><h4>City: <?php echo $city ?></h4></div>
            <div><h4>Zip: <?php echo $zip ?></h4></div>
            <div><h4>Country: <?php echo $country ?></h4></div>
            <div style="margin-top: 20px"></div>
            <a class="btn btn-outline-warning" href="edit_account.php">Edit</a>
            <div style="margin-top: 20px"></div>
           <?php endif ?>
            <?php if (isset($_SESSION['company'])):?>
                <div style="margin-top: 20px"></div>
                <div><h4>Photo: <?php echo "<img src='$photo' width='20%'/>" ?></h4></div>
                <div style="margin-top: 20px"></div>
                <div><h4>Company name: <?php echo $name ?></h4></div>
                <div><h4>Category: <?php echo $category ?></h4></div>
                <div><h4>Description: <?php echo $content ?></h4></div>
                <div><h4>Website: <?php echo $url ?></h4></div>
                <div style="margin-top: 20px"></div>
                <a class="btn btn-outline-warning" href="edit_company.php">Edit</a>
                <div style="margin-top: 20px"></div>
            <?php endif ?>

        </div>

         </div>


</div>
<?php include'footer.php';?>
