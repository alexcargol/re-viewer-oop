<?php include 'topbar.php';

if (isset($_SESSION['username'])) {
    $username = Entity\User::find($_SESSION['user_id']);
    $fname = $username->getFirstName();
    $lname = $username->getLastName();
    $address = $username->getAddress();
    $email = $username->getEmail();
    $city = $username->getCity();
    $zip = $username->getZip();
    $country = $username->getCountry();
    $photo = $username->getPhoto();


}


?>


<div id="body" class="container" style="margin-top:20px">
    <div class='wrapper row'>
        <h3>Edit account details for:<b> <?php echo $email ?></b> </h3>
        <div class='col-sm-12 col-lg-9'>
            <div style="margin-top: 20px"></div>
            <form id="edit_account" name="edit_account" action="processEditAccount.php" method="POST">
            <div><h4>Photo: <?php echo "<img src='$photo' />" ?></h4></div>
            <div style="margin-top: 20px"></div>
            <div><label><h4>First Name:</h4></label>
                <input type="text" id="fname" placeholder="<?php echo $fname ?>" name="fname" class="form-control">
            </div>
            <div><label><h4>Last Name: </h4></label>
                <input type="text" id="lname" placeholder="<?php echo $lname ?>" name="lname" class="form-control">
            </div>
            <div><label><h4>Address: </h4></label>
                <input type="text" id="address" placeholder="<?php echo $address ?>" name="address" class="form-control">
            </div>
            <div><label><h4>City: </h4></label>
                <input type="text" id="city" placeholder="<?php echo $city ?>" name="city" class="form-control">
            </div>
            <div><label><h4>Zip: </h4></label>
                <input type="text" id="zip" placeholder="<?php echo $zip ?>" name="zip" class="form-control">
            </div>
            <div><label><h4>Country: </h4></label>
                <input type="text" id="country" placeholder="<?php echo $country ?>" name="country" class="form-control">
            </div>
                <div><label><h4>Photo Url: </h4></label>
                    <input type="text" id="photo" name="photo" class="form-control">
                </div>
                <div style="margin-top: 20px"></div>

                <a class="btn btn-outline-success" onclick="document.getElementById('edit_account').submit();">Save</a>
                <div style="margin-top: 20px"></div>
                <h3>Change Password: </h3>
                <div style="margin-top: 20px"></div>
                <div><label><h4>Old Password: </h4></label>
                    <input type="password" id="oldpass" name="oldpass" class="form-control">
                </div>
                <div><label><h4>New Password: </h4></label>
                    <input type="password" id="newpass1" name="newpass1" class="form-control">
                </div>
                <div><label><h4>New Password: </h4></label>
                    <input type="password" id="newpass2" name="newpass2" class="form-control">
                </div>

            <div style="margin-top: 20px"></div>
            <a class="btn btn-outline-success" onclick="document.getElementById('edit_account').submit();">Save</a>
            </form>
            <div style="margin-top: 20px"></div>
        </div>
 </div>


</div>
<?php include'footer.php';?>
