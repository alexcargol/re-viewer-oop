<?php include 'topbar.php';
if (isset($_SESSION['company']) || isset($_SESSION['username'])) { header('Location: ../dashboard.php');}
if (isset($_GET['message'])) { $message=($_GET['message']);
        if ($message == 1) {echo "<div class='alert alert-danger' role='alert'><center>
            Wrong username or password!</center>
                                </div>";}
    if ($message == 2){ echo '<script type="text/javascript">
     alert("Username successfully created, now you can login.")</script>';}

}
?>
    <div class="example-wrapper">

        <div class="row">
            <div class="col-12" style="margin-bottom: 20px"></div>
            <div class="col-2"></div>
            <div class="col-8 card m-3 p-3">
                <h3>Business Login</h3>

                <form action="companyLogin.php" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                    <a class="btn btn-outline-success" href="RegisterCompany.php">Register</a>
                </form>

            </div>
            <div class="col-2"></div>
        </div>
        <div class="col-12" style="margin-bottom: 20px"></div>
    </div>
<?php include'footer.php';?>