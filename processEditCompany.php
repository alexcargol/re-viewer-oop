<?php include 'config.php';

$company = Entity\Company::find($_SESSION['user_id']);

if (isset($_POST['name']) && $_POST['name']!='') {
    $company->setName($_POST['name']);
}
if (isset($_POST['cat_id']) && $_POST['cat_id']!='') {
    $company->setCat_id($_POST['cat_id']);
}
if (isset($_POST['content']) && $_POST['content']!='') {
    $company->setContent($_POST['content']);
}

if (isset($_POST['picture']) && $_POST['picture']!='' ) {
    $company->setPicture($_POST['picture']);
}
if (isset($_POST['oldpass']) && $_POST['oldpass']!='' && isset($_POST['newpass1']) && isset($_POST['newpass2']) && ($_POST['newpass1']!='') && ($_POST['newpass2']!='') && ($_POST['newpass1'] == $_POST['newpass2']) && ($company->getPassword() == md5($_POST['oldpass']))) {
    $company->setPassword(md5($_POST['newpass1']));

}

$company->save();
header('Location: dashboard.php?message=1');

?>